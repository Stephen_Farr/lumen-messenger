-- Verison:     1.0
-- Author:      Stephen Farr <stephen.ross.farr@gmail.com>
-- Description: Creates the messenger tables

CREATE TABLE `users` (  `id` int(11) NOT NULL AUTO_INCREMENT,  `first_name` varchar(35) DEFAULT NULL,  `last_name` varchar(35) DEFAULT NULL,  `user_name` varchar(45) DEFAULT NULL,  `email` varchar(255) DEFAULT NULL,  `password` varchar(60) DEFAULT NULL,  `created_at` datetime DEFAULT NULL,  `updated_at` datetime DEFAULT NULL,  `token` varchar(255) DEFAULT NULL,  PRIMARY KEY (`id`),  UNIQUE KEY `id_UNIQUE` (`id`)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
CREATE TABLE `messages` (  `id` int(11) NOT NULL AUTO_INCREMENT,  `from_user_id` int(11) DEFAULT NULL,  `to_user_id` int(11) DEFAULT NULL,  `message` varchar(255) DEFAULT NULL,  `created_at` datetime DEFAULT NULL,  `updated_at` datetime DEFAULT NULL,  PRIMARY KEY (`id`),  UNIQUE KEY `id_UNIQUE` (`id`)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;