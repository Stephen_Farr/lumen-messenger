<?php

namespace App\Providers;

use App\Http\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.
        $this->app['auth']->viaRequest('api', function (Request $request) {
            $route = $request->getUri();
            $auth = $request->header('Authorization');

            if (strpos($route, 'login') !== false) {
                if (starts_with($auth, 'Basic')) {
                    $content = $this->getAuthorizationContent($auth);
                    $decoded_auth = base64_decode($content);
                    
                    $split_auth = explode(':', $decoded_auth);
                    
                    /** @var $user User */
                    $user = User::query()->where('email', '=', $split_auth[0])->first();

                    if (isset($user) && password_verify($split_auth[1], $user->password)) {
                        return $user;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                if (starts_with($auth, 'Token')) {
                    $token = $this->getAuthorizationContent($auth);
                    
                    $user = User::query()->where('token', '=', $token)->first();
                    return $user;
                } else {
                    return null;
                }
            }
        });
    }

    /**
     * Returns the token or basic auth content of the authorization header.
     *
     * @param $header
     * @return mixed
     */
    private function getAuthorizationContent($header) {
        return explode(' ', $header)[1];
    }
}
