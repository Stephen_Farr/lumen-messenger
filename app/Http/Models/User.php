<?php

/**
 * Created by IntelliJ IDEA.
 * User: stephenfarr
 * Date: 6/26/16
 * Time: 2:48 PM
 */

namespace App\Http\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Represents a user of the chat application
 *
 * Class User
 * @package App\Http\Models
 *
 * @property int $id Primary Key.
 * @property string $first_name Users First Name.
 * @property string $last_name Users Last Name.
 * @property string $user_name Users username.
 * @property string $email Users email.
 * @property string $password Users password.
 * @property Carbon $created_at Date the user was last created.
 * @property Carbon $updated_at Date the users details was last updated.
 * @property string $token Users Authorization token
 */
class User extends Model {
    protected $table = 'users';
    protected $dates = ['created_at', 'updated_at'];
    protected $hidden = ['password', 'token'];

    public function messages() {
        return $this->hasMany('App\Http\Models\Message', 'from_user_id');
    }

    public function getMessagesAttribute() {
        return $this->messages()->get();
    }

    public function setMessagesAttribute($messages) {
        $this->attributes['messages'] = $messages;
    }
}