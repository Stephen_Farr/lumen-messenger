<?php

/**
 * Created by IntelliJ IDEA.
 * User: stephenfarr
 * Date: 6/26/16
 * Time: 2:48 PM
 */

namespace App\Http\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Represents a message whether in a chat room or between two users directly.
 *
 * Class Message
 * @package App\Http\Models\Message
 *
 * @property int $id Primary Key
 * @property int $from_user_id ID of the user who sent the message.
 * @property int $to_user_id ID of the user who received the message.
 * @property string $message Message sent by the user
 * @property Carbon $created_at Date the message was originally sent.
 * @property Carbon $updated_at Date the message was last modified.
 */
class Message extends Model {
    protected $table = 'messages';
    protected $dates = ['created_at', 'updated_at'];
    protected $appends = ['from_user'];
    protected $hidden = ['from_user_id', 'to_user_id'];

    public function from_user() {
        return $this->belongsTo('App\Http\Models\User');
    }

    public function getFromUserAttribute() {
        return $this->from_user()->first();
    }
}