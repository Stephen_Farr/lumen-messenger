<?php
/**
 * Created by IntelliJ IDEA.
 * User: stephenfarr
 * Date: 6/26/16
 * Time: 5:37 PM
 */

namespace App\Http\Controllers\Message;


use App\Http\Controllers\AuthenticatedController;
use App\Http\Models\Message;
use Illuminate\Http\Request;

class MessageController extends AuthenticatedController {
    public function messages(Request $request) {
        $page = $request->has('page') ? $request->input('page') : '';
        $per_page = $request->has('per_page') ? $request->input('per_page') : '';

        if (empty($page)) {
            return response()->json(['message' => 'Missing parameter page.', 'code' => 400], 400);
        }

        if (empty($per_page)) {
            return response()->json(['message' => 'Missing parameter per_page', 'code' => 400]. 400);
        }

        $messages = Message::whereNull('to_user_id')->paginate($per_page)->sortBy('created_at');

        return response()->json($messages->values());
    }

    public function sendMessage(Request $request) {
        $user = \Auth::user();
        $send_to = $request->has('send_to_user') ? $request->input('send_to_user') : null;
        $message_sent = $request->has('message') ? $request->input('message') : '';

        if (empty($message_sent)) {
            return response()->json(['message' => 'Message is required', 'code' => 400], 400);
        }

        $message = new Message();
        $message->message = $message_sent;
        $message->to_user_id = $send_to;
        $message->from_user_id = $user->id;
        $message->save();

        return response()->json();
    }
}