<?php
/**
 * Created by IntelliJ IDEA.
 * User: stephenfarr
 * Date: 6/26/16
 * Time: 3:05 PM
 */

namespace App\Http\Controllers\User;

use App\Http\Controllers\AuthenticatedController;
use App\Http\Models\Message;
use App\Http\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class UserController extends AuthenticatedController {
    public function users(Request $request) {
        return response()->json(User::all()->sortBy('last_name')->values());
    }

    public function search(Request $request) {
        $keywords = $request->has('keywords') ? $request->input('keywords') : '';
        if (empty($keywords)) {
            return response()->json(['message' => 'Keywords must be supplied.', 'code' => 400], 400);
        }

        $result = User::where('first_name', 'like', '%' . $keywords . '%')
                    ->orWhere('last_name', 'like', '%' . $keywords . '%')->get();

        return response()->json($result);
    }
    
    public function userMessages(Request $request) {
        $user = \Auth::user();

        $std_class_ids = \DB::table('messages')->where('to_user_id', '=', $user->id)->groupBy('from_user_id')->get(['from_user_id']);

        $ids = new Collection();
        foreach ($std_class_ids as $std_class) {
            $ids->push($std_class->from_user_id);
        }
        
        $users = User::query()->findMany($ids->toArray());
        
        /** @var $from_user User */
        $result = new Collection();
        foreach ($users as $from_user) {
            $messages = Message::where('from_user_id', '=', $from_user->id)->where('to_user_id', '=', $user->id)->get();
            $from_user->setMessagesAttribute($messages);

            $result->push($from_user);
        }

        return response()->json($result);
    }
}