<?php
/**
 * Created by IntelliJ IDEA.
 * User: stephenfarr
 * Date: 6/26/16
 * Time: 5:38 PM
 */

namespace App\Http\Controllers;


class AuthenticatedController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }
}