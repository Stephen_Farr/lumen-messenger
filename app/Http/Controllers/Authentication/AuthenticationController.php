<?php
/**
 * Created by IntelliJ IDEA.
 * User: stephenfarr
 * Date: 6/26/16
 * Time: 3:06 PM
 */

namespace App\Http\Controllers\Authentication;


use App\Http\Controllers\Controller;
use App\Http\Models\User;
use Illuminate\Http\Request;

class AuthenticationController extends Controller {
    public function __construct() {
        $this->middleware('auth', ['only' => ['login']]);
    }

    public function login(Request $request) {
        $token = uniqid('messenger_', true);
        
        $user = \Auth::user();
        $user->token = $token;
        $user->save();

        return response()->json(\Auth::user()->setVisible(['id',
                                                              'first_name',
                                                              'last_name',
                                                              'user_name',
                                                              'email',
                                                              'created_at',
                                                              'updated_at',
                                                              'token']));
    }
    
    public function register(Request $request) {
        $user = $request->has('user') ? $request->input('user') : null;
        if (!isset($user)) {
            return response()->json(['message' => 'Missing body', 'code' => 400], 400);
        }

        $first_name = $request->has('user.first_name') ? $user['first_name'] : '';
        $last_name = $request->has('user.last_name') ? $user['last_name'] : '';
        $username = $request->has('user.username') ? $user['username'] : '';
        $password = $request->has('user.password') ? $user['password'] : '';
        $password_confirmation = $request->has('user.password_confirmation') ? $user['password_confirmation'] : '';

        if (empty($password) || empty($password_confirmation) || $password != $password_confirmation) {
            return response()->json(['message' => 'Passwords do not match.', 'code' => 401], 401);
        }

        $existing_user = User::query()->where('email', '=', $username)->first();
        if (isset($existing_user)) {
            return response()->json(['message' => 'User already exists', 'code' => 401], 401);
        }
        
        $new_user = new User();
        $new_user->first_name = $first_name;
        $new_user->last_name = $last_name;
        $new_user->user_name = $username;
        $new_user->password = password_hash($password, PASSWORD_BCRYPT);
        $new_user->email = $username;
        $new_user->save();

        return response()->json(['user' => $new_user->setVisible(['id',
                                                                     'first_name',
                                                                     'last_name',
                                                                     'user_name',
                                                                     'email',
                                                                     'created_at',
                                                                     'updated_at',
                                                                     'token'])]);
    }
}