<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

//Authentication Endpoints
$app->post('/auth/login', 'Authentication\AuthenticationController@login');
$app->post('/auth/register', 'Authentication\AuthenticationController@register');

//Messages
$app->get('/messages', 'Message\MessageController@messages');
$app->post('/messages/send', 'Message\MessageController@sendMessage');

//Users
$app->get('/users/list', 'User\UserController@users');
$app->get('/users/search', 'User\UserController@search');
$app->get('/users/messages', 'User\UserController@userMessages');